Scene Segmentation arrow for Kwiver.

to install this arrow "pip install ."

to use the arrow in a pipeline, see the examples directory readme.

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
