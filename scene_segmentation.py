from __future__ import print_function
from kwiver.vital.algo import ImageFilter
from PIL import Image as PILImage
from kwiver.vital import vital_logging
# the output of this arrow is an image which must be stored inside am
# implementation independent wrapper (ImageContainer)
from kwiver.vital.types import ImageContainer
from kwiver.vital.types import Image
import os
import sys
import cv2
import numpy
import torch
from torch.nn import functional as F


logger = vital_logging.getLogger(__name__)

# The ImageFilter class is the process that this arrow implements. It takes a single image
# in and produces a single image out per pass. Each pass is triggered by whatever source of
# images comes earlier in the pipeline.
class SceneSegmentation(ImageFilter):
    
    def __init__(self):
        ImageFilter.__init__(self)

    def get_configuration(self):
        # Inherit from the base class
        cfg = super(ImageFilter, self).get_configuration()
        return cfg

    # configuration options are established in ht epipeline segment that instantiates this arrow.
    def set_configuration( self, cfg_in ):
        cfg = self.get_configuration()
        cfg.merge_config(cfg_in)
        self.X = int(cfg.get_value("X"))
        self.Y = int(cfg.get_value("Y"))
        meanstring = cfg.get_value("mean")
        self.mean = [float(s) for s in meanstring.split(',')]
        stdstring = cfg.get_value("std")
        self.std = [float(s) for s in stdstring.split(',')]
        self.hrnetpath = cfg.get_value( "hrnet_path" )
        sys.path.insert(0, self.hrnetpath)
        import models
        import datasets
        from config.default import _C as hrnetconfig
        
        #Path to configuration file for HRNet. This file sets the hyper parameters of the model
        self.config_path = cfg.get_value( "hrnet_config_path" );

        # Path to the saved model weights
        self.model_file = cfg.get_value( "hrnet_model_file" );

        # Merge the model specific cofig file with the default conf file
        hrnetconfig.merge_from_file(self.config_path)

        # ADAPT input size
        hrnetconfig.TEST.IMAGE_SIZE = [self.X, self.Y]
        hrnetconfig.TEST.BASE_SIZE = self.X
        hrnetconfig.TEST.BATCH_SIZE_PER_GPU = 1

        # Load the model graph
        self.model = eval('models.'+hrnetconfig.MODEL.NAME + '.get_seg_model')(hrnetconfig)

        # Load the pretrained model weights
        pretrained_dict = torch.load(self.model_file)
        model_dict = self.model.state_dict()
        pretrained_dict = {k[6:]: v for k, v in pretrained_dict.items() if k[6:] in model_dict.keys()}
        model_dict.update(pretrained_dict)
        self.model.load_state_dict(model_dict)

        self.use_gpu = cfg.get_value( "useGPU" )

        # Transfer model to GPU
        if self.use_gpu:
            self.model = self.model.cuda()

        self.scriptspath=cfg.get_value( "scripts_path" )

        sys.path.insert(0, self.scriptspath)
        from cityscapesscripts.helpers.labels import labels

        self.label_map = {label.trainId : label.name for label in labels if not label.ignoreInEval}


    def check_configuration( self, cfg):
        return True

    # The ImageFilter base class defines a filter function that must be implemented.
    def filter(self, image_c):
        # Preprocessing steps
        image_in = cv2.resize(image_c.image().asarray(), (self.X, self.Y))
        im = image_in / 255.0
        im -= self.mean
        im /= self.std
        im = im.transpose([2,0,1])
        im = numpy.float32(im)
        if self.use_gpu:
            model_in = torch.tensor(im).cuda().unsqueeze(0)
        else:
            model_in = torch.tensor(im).unsqueeze(0)
        # Forward inference
        out = self.model(model_in)

        # Resize model predictions to input size
        preds = F.upsample(out, [self.Y,self.X], mode='bilinear')

        # Copy preds back to cpu
        preds = preds.detach().cpu().numpy()[0]

        # Predictions have shape [num_classes=19, width, height]
        # Each pixel contains 19 probabilities corrosponding to p(class k)
        #
        # We can simply take the argmax across the channels to reduce the tensor to a single
        # label image.
        preds = numpy.asarray(numpy.argmax(preds, axis=0), dtype=numpy.uint8)

        return ImageContainer(Image(preds))

# The __vital_algorithm_register__ establishes an entry point that kwiver and plugin_explorer use to
# identify usable plugins.
def __vital_algorithm_register__():
    from kwiver.vital.algo import algorithm_factory
    # Register Algorithm
    # the name indicates the name that is used to instantiate this type of image_filter.
    implementation_name  = "SceneSegmentation"
    # check if the type is already instantiated somewhere else.
    if algorithm_factory.has_algorithm_impl_name( SceneSegmentation.static_type_name(), implementation_name):
        return
    algorithm_factory.add_algorithm( implementation_name, "Segment the image according to the trained NN.", SceneSegmentation )
    algorithm_factory.mark_algorithm_as_loaded( implementation_name )
